

#SYN:xhrade08

use POSIX;
#use strict;
use warnings;

# navratove kody namisto magic numbers
use constant {
	SUCCESS => 0,
	ERR_PARAMS => 1,
	ERR_INPUT => 2,
	ERR_OUTPUT => 3,
	ERR_INPUT_FORMAT => 4,
};

#pocitadlo parametru
%params= ("help" => 0, "format" => 0, "input" => 0, "output" => 0, "br" => 0);
$format= "";
$input= "";
$output= "";

if (@ARGV == 0) {print STDERR "Chyba: Chybi parametry.\n"; exit(ERR_PARAMS);} #zadne parametry
if (@ARGV > 5) {print STDERR "Chyba: Prilis mnoho parametru.\n"; exit(ERR_PARAMS);} #moc parametru

#projedou se vsechny parametry a inkrementuji se jejich vyskyty 
foreach $param (@ARGV) 
{
   	if ($param eq "--help") {$params{"help"}++;}
   	elsif ($param =~ /^--format=/) {
		$params{"format"}++;
		$format=substr($param,9);
		}
   	elsif ($param =~ /^--input=/) {
		$params{"input"}++;
		$input=substr($param,8);
		}
   	elsif ($param =~ /^--output=/) {
		$params{"output"}++;
		$output=substr($param,9);
		}
   	elsif ($param eq "--br") {$params{"br"}++;}
	else {print STDERR "Chyba: Parametr $param neni podporovan.\n"; exit(ERR_PARAMS);}
}

if ((@ARGV == 1) && ($params{"help"} == 1))
{
	# VYTISTENI NAPOVEDY:
	print "SYN - obarvovani syntaxe \n
--help\n	vypise tuto napovedu\n
--format=filename\n	ur�en� formatovaciho souboru. Soubor bude obsahovat libovolne mnozstvi formatovacich zaznamu. Formatovaci zaznam se bude skladat z regularniho vyrazu vymezujiciho formatovany text a tagu pro formatovani tohoto textu.\n
--input=filename\n	urceni vstupniho souboru v kodovani UTF-8.\n
--output=filename\n	urceni vystupniho souboru opet v kodovani UTF-8 s naformatovanym vstupnim textem. Formatovani bude na vystupu realizovano nekterymi HTML elementy na popis formatovani. Regularni vyrazy ve formatovacim souboru urci, ktery text se naformatuje, a formatovaci informace prirazene k danemu vyrazu urci, jakym zpusobem se tento text naformatuje.\n
--br\n	prida element <br /> na konec kaydeho radku puvodniho vstupniho textu(az po aplikaci formatovacich tagu)\n";
	

}
elsif ((($params{"format"} == 1) || ($params{"format"} == 0)) && 
	(($params{"output"} == 1 ) || ($params{"output"} == 0)) &&
	(($params{"input"} == 1 ) || ($params{"input"} == 0)) &&
	(($params{"br"} == 1 ) || ($params{"br"} == 0)) &&
	($params{"help"}==0))
{
	# NORMALNI CINNOST PROGRAMU

	# otevreni vstupniho souboru/standardniho vstupu
	my $ifh; #input file handle
	if ($params{"input"}==1) {
		if (not open ($ifh, "<", $input))
		{
			print STDERR "Vstupni soubor \"$input\" se nepodari otevrit.\n"; 
			exit(ERR_INPUT);
		}
		#print "Vstupni soubor $input otevren OK\n";
		  
	} else {
		$ifh = *STDIN;
		#print "Vstupni se bere z STDIN\n";
	}
	
	# cely soubor se nacte do input_file
	while (<$ifh>) {$input_file .= $_;}
	 
	close $ifh unless ($params{"input"}==1);

	# ------------------------------------

	#kdyz je definovat --output, tak se otrevre, jinak se bude zapisovat na stdout
	my $ofh; #output file handle
	if ($params{"output"}==1) {
		if ( not open($ofh, '>', $output))
		{
			print STDERR "Chyba: vystupni soubor \"$output\" se nepodarilo otevrit.\n"; 
			exit(ERR_OUTPUT);
		}
		#print "Vystupni soubor $output otevren OK\n";
	} else {
		if ( not open($ofh, '>&', \*STDOUT)) 
		{
			print STDERR "Chyba: Nepodaril se zapis do STDOUT.\n"; 
			exit(ERR_OUTPUT);
		}
		#print "Vystup se pise na STDOUT OK\n";
	}

	# ------------------------------------

	# nacteni formatovaciho souboru do pameti
	if ($params{"format"}==1) 
	{
		# Format file handler
		if ( not open($ffh, "<", $format))
		{
			print STDERR "Chyba: Formatovaci soubor \"$format\" se nepodarilo otevrit.\n"; 
			exit(ERR_INPUT);
		}
		#print "Formatovaci soubor $format otevren OK\n";
		@lines = <$ffh>;
		close($ffh);


		my %changes = ();
		LINE: foreach $line (@lines)
		{
			# kontrola, zda je na radku text oddeleny tabulatorem 
			if (not $line =~ m/.+\t.+/)
			{
				print STDERR "Chyba: Chyba ve formatovacim souboru: chybi tabulator mezi RV a znackami na radku: \n$line"; 
				exit(ERR_INPUT_FORMAT);
			}

			# rozdeleni kazdeho radku formatovaciho souboru na <regexp> a <parametry>
			my $regexp = ''; my $tags = '';
		    ( $regexp, $tags) = split(/\t+/, $line, 2);
		  
			$open_tags = ""; # string se vsemi zacatecnimi tagy aplikovanymi u aktualniho regexpu
			$close_tags = "";
			if (!defined($tags) || !defined($regexp)) {next LINE;}


			chomp($tags); #ureze \n na konci radku
			@tags = split (/,[\t ]*/,$tags);
			
			# skladani vyslednych retezcu pro obarveni vstupniho souboru 
			foreach $tag (@tags) {
	   			if ($tag eq "bold") {
					$open_tags = $open_tags."<b>";
					$close_tags = "</b>".$close_tags;
				}
	   			elsif ($tag eq "italic") {
					$open_tags = $open_tags."<i>";
					$close_tags = "</i>".$close_tags;
				}
	   			elsif ($tag eq "underline") {
					$open_tags = $open_tags."<u>";
					$close_tags = "</u>".$close_tags;
				}
	   			elsif ($tag eq "teletype") {
					$open_tags = $open_tags."<tt>";
					$close_tags = "</tt>".$close_tags;
				}
	   			elsif ($tag =~ m/^color:[0-9A-F]{6}$/) {
					$open_tags = $open_tags."<font color=#".substr($tag,6,6).">";
					$close_tags = "</font>".$close_tags;
				}
	   			elsif ($tag =~ m/^size:[1-7]$/) {
					$open_tags = $open_tags."<font size=".substr($tag,5,1).">";
					$close_tags = "</font>".$close_tags;
				}
				else { 
					print STDERR "Chyba: Chyba ve formatovacim souboru: nevalidni formatovaci znacka \"$tag\".\n"; 
					exit(ERR_INPUT_FORMAT);
				}

			} 
			
			#print "\nREGEXP pred: $regexp \n";

			# kontrola validnosti regexpu:

			# detekce chybneho regexpu - tecka na zacatku nebo na konci retezce
			if ($regexp =~ m/(^\.|([^\%](\%\%)*)\.$)/) #
			{
				print STDERR "Chyba: Nevalidni regexp \"$regexp\": tecka na konci nebo zacatku retezce.\n";
				exit(ERR_INPUT_FORMAT);
			}

			# detekce chybneho regexpu - konkatenace prazdneho retezce
			if ($regexp =~ m/(([^\%]|^)(\%\%)*)\.(\.|\*|\+|\|)/)
			{
				print STDERR "Chyba: Nevalidni regexp \"$regexp\": konkatenace prazdneho retezce \n";
				exit(ERR_INPUT_FORMAT);
			}
			# detekce chybneho regexpu - spatna kombinace operatoru
			if ($regexp =~ m/(([^\%]|^)(\%\%)*)\|(\.|\|)/) 
			{
				print STDERR "Chyba: Nevalidni regexp \"$regexp\": spatna kombinace operatoru \n";
				exit(ERR_INPUT_FORMAT);
			}

			# detekce chybneho regexpu - nepovolene znaky za % 
			if ( $regexp =~ m/(([^\%]|^)(\%\%)*|^)\%[^(s|a|d|l|L|w|W|t|n|\.|\||\!|\*|\+|\(|\)|\%)]/)
			{
				print STDERR "Chyba: Nevalidni regexp \"$regexp\": nepovolene znaky za %\n";
				exit(ERR_INPUT_FORMAT);
			}

			# detekce chybneho regexpu - negace operatoru 
			if ( $regexp =~ m/(([^\%]|^)(\%\%)*|^)\!(\.|\||\!|\*|\+|\(|\))/)
			{
				print STDERR "Chyba: Nevalidni regexp \"$regexp\": negace operatoru.\n";
				exit(ERR_INPUT_FORMAT);
			}

			# detekce chybneho regexpu - nektere operatory za (
			if ( $regexp =~ m/(([^\%]|^)(\%\%)*|^)\((\.|\||\*|\+)/)
			{
				print STDERR "Chyba: Nevalidni regexp \"$regexp\": operator za (\n";
				exit(ERR_INPUT_FORMAT);
			}

			# detekce chybneho regexpu - nektere operatory pred )
			if ( $regexp =~ m/(([^\%]|^)(\%\%)*|^)(\.|\||\!)\)/)
			{
				print STDERR "Chyba: Nevalidni regexp \"$regexp\": operator pred )\n";
				exit(ERR_INPUT_FORMAT);
			}


# !!!!!!!!!!!!!!! potreba vsude predelat [^\%] na ([^\%](\%\%)*) napr %%%%j musi byt validni
			# uprava regexpu do perlove podoby
			if ($regexp =~ m/(([^\%]|^)(\%\%)*)\./)
				{ while ($regexp =~ s/(([^\%]|^)(\%\%)*)\./$1/) {;} } 		#	A.B -> AB

			#pridani \ pred veci co v perlovych regexp neco znamenaji, ale v IFJ ne \^$[]-?{}
			if ($regexp =~ m/((^|[^\%])(\%\%)*)(\x5C|\^|\$|\[|\]|\-|\?|\{|\})/) 				
				{
					while ($regexp =~ s/((^|[^\%\x01])(\%\%)*)(\x5C|\^|\$|\[|\]|\-|\?|\{|\})/$1\x01$4/ ) {;} 
					$regexp =~ s/\x01/\x5C/g; #nahrazeni znaku 1 za \
				}

			#if ($regexp =~ m/(([^\%](\%\%)*)|^)\!([^\%]|\%.)/ )
			#	{ while ($regexp =~ s/(([^\%](\%\%)*)|^)\!([^\%]|\%.)/$1\[^$4\]/) {;} } # resi negace !A -> [^A]
			if ($regexp =~ m/((([^\%]|^)(\%\%)*)|^)\!([^\%]|\%.)/ )
				{ while ($regexp =~ s/((([^\%]|^)(\%\%)*)|^)\!([^\%]|\%.)/$1\[^$5\]/) {;} } # resi negace !A -> [^A]

			if ( $regexp =~ m/(^|[^\\])\%(\%)/ )
				{ while  ($regexp =~ s/(^|[^\\])\%(\%)/$1\\$2/ ) {;} } # predela vsechny %% -> \%		

			if ( $regexp =~ m/(([^\\]|^)(\\\\)*|^)\%(d|t|n|\.|\||\!|\*|\+|\(|\))/) # predelani %neco na \neco pro neco= dtn.|!*+() 
				{ while ($regexp =~ s/(([^\\]|^)(\\\\)*|^)\%(d|t|n|\.|\||\!|\*|\+|\(|\))/$1\\$4/){;}} 

			if ( $regexp =~ m/(((([^\\]|^)|^)(\\\\)*)|^)\%l/ )
				{ while ($regexp =~ s/((([^\\]|^)(\\\\)*)|^)\%l/$1\[a-z\]/){;}} # %l -> [a-z]

			if ( $regexp =~ m/((([^\\]|^)(\\\\)*)|^)\%L/ )
				{ while ($regexp =~ s/((([^\\]|^)(\\\\)*)|^)\%L/$1\[A-Z\]/){;}} # %L -> [A-Z]

			if ( $regexp =~ m/((([^\\]|^)(\\\\)*)|^)\%w/ )
				{ while ($regexp =~ s/((([^\\]|^)(\\\\)*)|^)\%w/$1\[a-zA-Z\]/){;}} # %w -> [a-zA-Z]

			if ( $regexp =~ m/((([^\\]|^)(\\\\)*)|^)\%W/ )
				{ while ($regexp =~ s/((([^\\]|^)(\\\\)*)|^)\%W/$1\[a-zA-Z0-9\]/){;}} # %W -> [a-zA-Z0-9]
		
			if ( $regexp =~ m/((([^\\]|^)(\\\\)*)|^)\%a/ )
				{ while ($regexp =~ s/((([^\\]|^)(\\\\)*)|^)\%a/$1\./){;}} # %a -> .

			if ( $regexp =~ m/((([^\\]|^)(\\\\)*)|^)\%s/ )
				{ while ($regexp =~ s/((([^\\]|^)(\\\\)*)|^)\%s/$1\( \|\\t\|\\n\|\\r\|\\x0C\|\\x0B\)/){;}} # %s -> ( |\t|\n|\r|\f|\v) .

			if ($regexp =~ m/([^\x5C]|^)\[\^\.\]/ )
				{ while ($regexp =~ s/([^\x5C]|^)\[\^\.\]/$1\.\x5CA/) {;} } # resi ze [^.] se ma rovnat nicemu - nasimulovano .\A

			if ($regexp =~ m/([^\x5C]|^)\[\^\[A\x2DZ\]\]/ )
				{ while ($regexp =~ s/([^\x5C]|^)\[\^\[A\x2DZ\]\]/$1\[\^A-Z\]/) {;} } # [^[A-Z]] -> [^A-Z] aby negace fungovala
			if ($regexp =~ m/([^\x5C]|^)\[\^\[A\x2DZ\]\]/ )
				{ while ($regexp =~ s/([^\x5C]|^)\[\^\[a\x2Dz\]\]/$1\[\^a-z\]/) {;} } # [^[a-z]] -> [^a-z] aby negace fungovala
			if ($regexp =~ m/([^\x5C]|^)\[\^\[A\x2DZ\]\]/ )
				{ while ($regexp =~ s/([^\x5C]|^)\[\^\[a\x2DzA\x2DZ\]\]/$1\[\^a-zA-Z\]/) {;} } # [^[a-zA-Z]] -> [^a-zA-Z] aby negace fungovala
			if ($regexp =~ m/([^\x5C]|^)\[\^\[A\x2DZ\]\]/ )
				{ while ($regexp =~ s/([^\x5C]|^)\[\^\[a\x2DzA\x2DZ0\x2D\x39\]\]/$1\[\^a-zA-Z0-9\]/) {;} } # [^[a-zA-Z0-9]] -> [^a-zA-Z0-9] 

			#print "REGEXP po: $regexp \n";
		
			#validace vysledneho regexpu
			my $validation = eval { qr/$regexp/ };
			if ($@)
			{
				print STDERR "Chyba: Nevalidni regexp \"$regexp\"\n";
				exit(ERR_INPUT_FORMAT);
			}

			# pridavani zmen pro nalezene vyskyty ve vstupnim souboru, klic urcuje pozici a hodnota pridavany retezec
			if (defined $input_file)
			{
				while ( $input_file  =~ /$regexp/gs) 
				{
					#print $-[0]." ".$+[0]."\n" ; #pozice zacatku a konce najiteho regexpu
					$start = $-[0];
					$end = $+[0];
					if ($start != $end)
					{
						if (not exists $changes{ $start }) {$changes{$start}=""}
							$changes{$start}=$changes{$start}.$open_tags;
						if (not exists $changes{ $end }) {$changes{$end}=""}
							$changes{$end}=$close_tags.$changes{$end};
					}
				}
			}
		  
		}

		# kdyz je vsupni soubor prazdny, neni co by se zvyraznovalo -> bude i prazny vystup
		if (not defined $input_file)
		{
			close($ofh);
			exit(SUCCESS);
		}

		#zapsani vsech zmen do vstupniho souboru v pameti
		foreach $pos (reverse sort{$a <=> $b} keys(%changes))
		{
			substr($input_file,$pos,0)=$changes{$pos};  
		}
		
		# pridani <br /> na kazdy konec radku
		if ($params{"br"}==1) {$input_file =~ s/(\n)/<br \/>$1/g;}

	}

	print $ofh $input_file;
	close($ofh);

}
else
{
	# CHYBA V PARAMETRECH -> konec
	print STDERR "Chyba: Chybne parametry.\n";
	exit(ERR_PARAMS);
}

exit (SUCCESS);
